﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Mvc.Filters;
using Demo.Utilities;
using Demo.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Demo
{
	public class Startup
	{
		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services)
		{
			var connectionString = @"Server=.\SQLEXPRESS; Database=productdb; Integrated Security = true;";
			services.AddDbContext<ProductContext>(options => options.UseSqlServer(connectionString));

			services.AddMvc().AddMvcOptions(options =>
			{
				options.Filters.Add(new ExceptionHandler());
			}).AddJsonOptions(options =>
			{
				options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app)
		{
			app.UseStaticFiles();

			app.UseMvc();

			app.UseStatusCodePages();
		}
	}
}
