﻿using Demo.DataAccess;
using Demo.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Controllers
{
	[Route("/api/product")]
    public class ProductController : Controller
    {
		private ProductContext context;

		// Dependency injection
		public ProductController(ProductContext context)
		{
			this.context = context;
		}

		[HttpGet]
		public IEnumerable<ProductModel> Get()
		{
			return context.Producten.Where(x => x.Product.StartsWith("i")).ToList();
		}

		[HttpPost]
		public ProductModel Post([FromBody]ProductModel newProduct)
		{
			if(ModelState.IsValid)
			{
				context.Producten.Add(newProduct);
				context.SaveChanges();

				return newProduct;
			}

			throw new ArgumentException("Dat is geen geldig product");
		}
    }
}