﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Models
{
    public class ProductModel
    {
		public int Id { get; set; }

		public string Product { get; set; }

		public decimal BTW { get; set; }
	}
}
