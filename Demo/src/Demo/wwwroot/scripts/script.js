﻿angular.module('demoApp', []);

angular.module('demoApp').controller('productCtrl', function ($http) {

	this.getProducten = () => {
		$http.get('api/product').then(response => {
			this.producten = response.data;
		});
	};

	this.addProduct = () => {
		$http.post('api/product', this.newProduct).then(response => {
			this.producten.push(response.data);
			delete this.newProduct;
		});
	};
});